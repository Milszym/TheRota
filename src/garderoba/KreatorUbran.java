package garderoba;

import garderoba.ubrania.Buty;
import garderoba.ubrania.Koszulka;
import garderoba.ubrania.Spodnie;
import garderoba.ubrania.Sukienka;

import java.util.Scanner;

public class KreatorUbran {

    public Buty utworzButyZAtrybutami() {
        Scanner scanner = new Scanner(System.in);

        Buty noweButy = new Buty();

        System.out.println("Wpisz firmę: ");
        noweButy.firma = scanner.nextLine();

        System.out.println("Wpisz rozmiar: ");
        noweButy.rozmiar = Integer.valueOf(scanner.nextLine());

        return noweButy;
    }

    public Koszulka utworzKoszulkeZAtrybutami() {
        Scanner scanner = new Scanner(System.in);

        Koszulka nowaKoszulka = new Koszulka();

        System.out.println("Wpisz firmę: ");
        nowaKoszulka.firma = scanner.nextLine();

        System.out.println("Wpisz rozmiar: ");
        nowaKoszulka.rozmiar = Integer.valueOf(scanner.nextLine());

        return nowaKoszulka;
    }

    public Spodnie utworzSpodnieZAtrybutami() {
        Scanner scanner = new Scanner(System.in);

        Spodnie noweSPodnie = new Spodnie();

        System.out.println("Wpisz firmę: ");
        noweSPodnie.firma = scanner.nextLine();

        System.out.println("Wpisz rozmiar: ");
        noweSPodnie.rozmiar = Integer.valueOf(scanner.nextLine());

        return noweSPodnie;
    }

    public Sukienka utworzSukienkeZAtrybutami() {
        Scanner scanner = new Scanner(System.in);

        Sukienka nowaSukienka = new Sukienka();

        System.out.println("Wpisz firmę: ");
        nowaSukienka.firma = scanner.nextLine();

        System.out.println("Wpisz rozmiar: ");
        nowaSukienka.rozmiar = Integer.valueOf(scanner.nextLine());

        return nowaSukienka;
    }

}
