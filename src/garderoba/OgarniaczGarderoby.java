package garderoba;

import garderoba.ubrania.Buty;
import garderoba.ubrania.Koszulka;
import garderoba.ubrania.Spodnie;
import garderoba.ubrania.Sukienka;

public class OgarniaczGarderoby {

    private int licznikButow = 0;

    private int licznikKoszulek = 0;

    private int licznikSpodni = 0;

    private int licznikSukienek = 0;


    private Buty[] listaButow = new Buty[20];

    private Koszulka[] listaKoszulek = new Koszulka[20];

    private Spodnie[] listaSpodni = new Spodnie[20];

    private Sukienka[] listaSukienek = new Sukienka[20];



    public void dodajButy(Buty noweButy) {
        listaButow[licznikButow] = noweButy;
        licznikButow = licznikButow + 1;
    }

    public void dodajKoszulke(Koszulka nowaKoszulka) {
        listaKoszulek[licznikKoszulek] = nowaKoszulka;
        licznikKoszulek = licznikKoszulek + 1;
    }

    public void dodajSpodnie(Spodnie noweSpodnie) {
        listaSpodni[licznikSpodni] = noweSpodnie;
        licznikSpodni = licznikSpodni + 1;
    }

    public void dodajSukienke(Sukienka nowaSukienka) {
        listaSukienek[licznikSukienek] = nowaSukienka;
        licznikSukienek = licznikSukienek + 1;
    }




    public void wypiszWszystkieLiczniki() {
        System.out.println("Liczba butow wynosi: " + licznikButow);
        System.out.println("Liczba koszulek wynosi: " + licznikKoszulek);
        System.out.println("Liczba spodni wynosi: " + licznikSpodni);
        System.out.println("Liczba sukienek wynosi: " + licznikSukienek);
    }



    public void wypiszWszystkieCiuchy() {
        System.out.println("Buty: ");
        for(int i=0; i< licznikButow; i++) {
            Buty aktualneButy = listaButow[i];
            System.out.println(String.valueOf(i+1) + ". " + aktualneButy.firma + " " + aktualneButy.rozmiar);
        }

        System.out.println("Koszulki: ");
        for(int i=0; i< licznikKoszulek; i++) {
            Koszulka aktualnaKoszulka = listaKoszulek[i];
            System.out.println(String.valueOf(i+1) + ". " + aktualnaKoszulka.firma + " " + aktualnaKoszulka.rozmiar);
        }

        System.out.println("Spodnie: ");
        for(int i=0; i< licznikSpodni; i++) {
            Spodnie aktualneSpodnie = listaSpodni[i];
            System.out.println(String.valueOf(i+1) + ". " + aktualneSpodnie.firma + " " + aktualneSpodnie.rozmiar);
        }

        System.out.println("Sukienki: ");
        for(int i=0; i< licznikSukienek; i++) {
            Sukienka aktualnaSukienka = listaSukienek[i];
            System.out.println(String.valueOf(i+1) + ". " + aktualnaSukienka.firma + " " + aktualnaSukienka.rozmiar);
        }
    }

}
