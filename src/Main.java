import garderoba.KreatorUbran;
import garderoba.OgarniaczGarderoby;
import garderoba.ubrania.Buty;
import garderoba.ubrania.Koszulka;
import garderoba.ubrania.Spodnie;
import garderoba.ubrania.Sukienka;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.wypiszInstrukcjeDlaUzytkownika();
        main.uruchomPetleCzekajacaNaUzytkownika();
    }

    private void wypiszInstrukcjeDlaUzytkownika() {
        System.out.println("Oto lista komend: ");
        System.out.println("Buty - dodaje buty");
        System.out.println("Koszulka - dodaje koszulke");
        System.out.println("Spodnie - dodaje spodnie");
        System.out.println("Sukienka - dodaje sukienke");
        System.out.println("Garderoba - wypisuje ilość wszystkich ubrań");
        System.out.println("Wypisz wszystko - wypisuje wszystkie ubrania");
        System.out.println("Koniec - kończy program.");
        System.out.println();
    }

    private void uruchomPetleCzekajacaNaUzytkownika() {

        OgarniaczGarderoby ogarniaczGarderoby = new OgarniaczGarderoby();

        KreatorUbran kreatorUbran = new KreatorUbran();

        Scanner zczytywaczKlawiatury = new Scanner(System.in);

        while (true) {
            System.out.print("Proszę wpisać komendę: ");
            String wpisanaKomendaUzytkownika = zczytywaczKlawiatury.nextLine();


            if (wpisanaKomendaUzytkownika.equals("Buty")) {
                Buty noweButy = kreatorUbran.utworzButyZAtrybutami();
                ogarniaczGarderoby.dodajButy(noweButy);
                System.out.println("Pomyślnie dodano buty.");
            }

            else if (wpisanaKomendaUzytkownika.equals("Koszulka")) {
                Koszulka nowaKoszulka = kreatorUbran.utworzKoszulkeZAtrybutami();
                ogarniaczGarderoby.dodajKoszulke(nowaKoszulka);
                System.out.println("Pomyślnie dodano koszulkę.");
            }

            else if (wpisanaKomendaUzytkownika.equals("Spodnie")) {
                Spodnie noweSpodnie = kreatorUbran.utworzSpodnieZAtrybutami();
                ogarniaczGarderoby.dodajSpodnie(noweSpodnie);
                System.out.println("Pomyślnie dodano spodnie.");
            }

            else if (wpisanaKomendaUzytkownika.equals("Sukienka")) {
                Sukienka nowaSukienka = kreatorUbran.utworzSukienkeZAtrybutami();
                ogarniaczGarderoby.dodajSukienke(nowaSukienka);
                System.out.println("Pomyślnie dodano sukienkę.");
            }

            else if (wpisanaKomendaUzytkownika.equals("Garderoba")) {
                ogarniaczGarderoby.wypiszWszystkieLiczniki();
            }

            else if(wpisanaKomendaUzytkownika.equals("Wypisz wszystko")) {
                ogarniaczGarderoby.wypiszWszystkieCiuchy();
            }

            else if (wpisanaKomendaUzytkownika.equals("Koniec")) {
                break;
            }


        }
    }


}
